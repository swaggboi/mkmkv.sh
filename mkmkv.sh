#!/usr/bin/env sh

# Daniel Bowling <swaggboi@slackware.uk>
# Dec 2020

# The double-quotes around the directory/files names are important
# because torrent'd files have some ridiculous file names

# Is this what you want??
while :; do
    cat <<EOF
This script will attempt to traverse the current directory (and any
child directories) for video files to convert to .mkv format with
ffmpeg. Continue?? (y/N)
EOF
     read -r answer
     case "$answer" in
         Y*|y*)
             break
             ;;
         *)
             echo "bye"
             exit 65
             ;;
     esac
done

# Define this recursive function
conv_vid() {
    # Begin a big ol loop for every file
    for file in "$PWD"/*; do
        if [ -f "$file" ]; then
            case "$file" in
                *".M4V"|*".m4v")
                    oldfile=$file
                    newfile=${oldfile/+(.M4V|.m4v)/.mkv}
                    ;;
                *".AVI"|*".avi")
                    oldfile=$file
                    newfile=${oldfile/+(.AVI|.avi)/.mkv}
                    ;;
                *".MKV"|*".mkv")
                    oldfile=$file
                    newfile=${oldfile/+(.MKV|.mkv)/.NEW.mkv}
                    ;;
                *)
                    # Next iteration of big ol loop
                    continue
                    ;;
            esac

            if [ "$oldfile" ]; then
                # Convert the file now
                ffmpeg -i "$oldfile" -vcodec libx265 -crf 28 "$newfile"
            fi
        elif [ -d "$file" ]; then
            (
                cd "$file" || exit 64
                conv_vid
            )
        fi
    done
}

# Call the function
conv_vid
